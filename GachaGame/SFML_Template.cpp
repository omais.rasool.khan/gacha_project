#include <iostream>
#include <fstream>
#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"
#include <string>
#include <windows.h>

#include <ctime>
#include "json.hpp"
#include "gameManager.h"
#include "Draw.h"

//used to draw player hand and board cards
void draw(gameManager* playerManager, sf::RenderWindow* window, sf::Texture &cardFrame,
	std::vector<sf::Sprite*> &cardCharactersSprite,
	std::vector<sf::Sprite*> &cardFrameSprite, std::vector<sf::Texture*> &cardTexture,
	std::vector<std::vector<float>> &cardPositions,
	std::vector<sf::Sprite*> &playerSprite,
	std::vector<sf::Sprite*> &computerSprite,
	std::vector<sf::Sprite*> &cardFrameSpritePlayerBoard,
	std::vector<sf::Sprite*> &cardFrameSpriteComputerBoard,
	std::vector<sf::Texture*> &CompCardTexture)
{
	for (int i = 0; i < playerManager->getHand()->getHand().size(); i++)
	{
		// CREATE textures for card characters for each card in hand
			// draw cards with set origin here
		float x_coord = cardPositions[i][0];
		float y_coord = cardPositions[i][1];

		//set and load texture
		sf::Texture* texture = new sf::Texture;
		if (texture->loadFromFile("characters/" + playerManager->getHand()->getHand()[i]->getCardName() + ".png") == false) {
			std::cout << "TEXTURE FAILED TO LOAD----------";
		}
		//create and load charactersprite
		sf::Sprite* cardSprite = new sf::Sprite;
		cardSprite->setTexture(*texture); //set texture in sprite
		cardSprite->setOrigin(sf::Vector2f(texture->getSize().x * 0.5f + x_coord, texture->getSize().y * 0.8f + y_coord));

		//create and load frame sprite
		sf::Sprite* frameSprite = new sf::Sprite;
		frameSprite->setTexture(cardFrame);
		frameSprite->setOrigin(sf::Vector2f(cardFrame.getSize().x * 0.5f + x_coord, cardFrame.getSize().y * 0.5f + y_coord));
		//push all components
		cardFrameSprite.push_back(frameSprite);
		cardTexture.push_back(texture);
		cardCharactersSprite.push_back(cardSprite);

		cardCharactersSprite[i]->setTexture(*texture);


		if (!playerSprite.empty())
			for (int j = 0; j < playerSprite.size(); j++)
			{
				playerSprite[j]->setOrigin(sf::Vector2f(cardFrame.getSize().x * (j * -0.4f) + 330.0f, cardFrame.getSize().y * 1.8f - 250.0f));
				cardFrameSpritePlayerBoard[j]->setOrigin(sf::Vector2f(cardFrame.getSize().x * j * -0.4f + 350.0f, cardFrame.getSize().y * 1.8f + -230.0));
				window->draw(*playerSprite[j]);
				window->draw(*cardFrameSpritePlayerBoard[j]);
			}

		if (!computerSprite.empty())
		{
			for (int j = 0; j < computerSprite.size(); j++)
			{
				computerSprite[j]->setOrigin(sf::Vector2f(cardFrame.getSize().x * (j * -0.4f) + 330.0f, cardFrame.getSize().y * 1.8f - 10.0f));
				computerSprite[j]->setTexture(*CompCardTexture[j]);
				cardFrameSpriteComputerBoard[j]->setOrigin(sf::Vector2f(cardFrame.getSize().x * j * -0.4f + 350.0f, cardFrame.getSize().y * 1.8f + 0.0));
				window->draw(*computerSprite[j]);
				window->draw(*cardFrameSpriteComputerBoard[j]);
			}
		}


		window->draw(*cardCharactersSprite[i]);
		window->draw(*cardFrameSprite[i]);

	}
		
}

void playGame(gameManager* playerManager, gameManager* computerManager)
{
	//
	


	sf::RenderWindow* window = new sf::RenderWindow(sf::VideoMode(1024, 768), "Gacha Game");
	window->setFramerateLimit(60);

	sf::View view; //reset view to dictate where the origin is
	float halfWidth = window->getSize().x * 0.5f;
	float halfHeight = window->getSize().y * 0.5f;
	view.reset(sf::FloatRect(-halfWidth, -halfHeight, halfWidth * 2.0f, halfHeight * 2.0f));
	window->setView(view); //set this view for the window

	sf::Texture line;
	line.loadFromFile("line.jpg");
	sf::Sprite Wline;
	Wline.setTexture(line); //set texture in sprite
	Wline.setOrigin(sf::Vector2f(line.getSize().x * 0.5f, (line.getSize().y * 0.5f + 50.0f)));
	

	bool winner = false;
	int turn = rand() % 2;

	int playerLives = 2;
	int computerLives = 2;

	//create and load texture for card holder
		//create and load texture for melee, range, seige
		//create and load font, and set text for comp and player and origin here
		// CREATE different vectors to store sprites for characters

	sf::Font font;
	if (font.loadFromFile("RobotoCondensed-Regular.ttf")) {
		std::cout<<"Font loaded"<<std::endl;
	}

	sf::Text passButton;
	passButton.setFont(font);
	passButton.setCharacterSize(30);
	passButton.setFillColor(sf::Color::Red);
	passButton.setStyle(sf::Text::Regular);
	passButton.setString("PASS TURN");
	passButton.setOrigin(sf::Vector2f(passButton.getLocalBounds().width * 0.5f + 400.0f, passButton.getLocalBounds().height * 0.5f - 290.0f));

	//create text if font loads... texts for cards in player hand
	sf::Text computerScoreText;
	computerScoreText.setFont(font);
	computerScoreText.setCharacterSize(20);
	computerScoreText.setFillColor(sf::Color::Red);
	computerScoreText.setStyle(sf::Text::Regular);
	//text0.setString("HEllo world");
	//set text origin
	computerScoreText.setOrigin(sf::Vector2f(computerScoreText.getLocalBounds().width * 0.5f + 300.0f, computerScoreText.getLocalBounds().height * 0.5f + 320.0f));

	sf::Text playerScoreText;
	playerScoreText.setFont(font);
	playerScoreText.setCharacterSize(20);
	playerScoreText.setFillColor(sf::Color::Red);
	playerScoreText.setStyle(sf::Text::Regular);
	//text0.setString("HEllo world");
	//set text origin
	playerScoreText.setOrigin(sf::Vector2f(playerScoreText.getLocalBounds().width * 0.5f + 300.0f, playerScoreText.getLocalBounds().height * 0.5f - 290.0f));

	sf::Text winnerStatus;
	winnerStatus.setFont(font);
	winnerStatus.setCharacterSize(50);
	winnerStatus.setFillColor(sf::Color::Red);
	winnerStatus.setStyle(sf::Text::Regular);
	//text0.setString("HEllo world");
	//set text origin
	winnerStatus.setOrigin(sf::Vector2f(winnerStatus.getLocalBounds().width * 0.5f + 120.0f, winnerStatus.getLocalBounds().height * 0.5f - 0.0f));

	sf::Text roundStatus;
	roundStatus.setFont(font);
	roundStatus.setCharacterSize(35);
	roundStatus.setFillColor(sf::Color::Red);
	roundStatus.setStyle(sf::Text::Regular);
	//text0.setString("HEllo world");
	//set text origin
	roundStatus.setOrigin(sf::Vector2f(roundStatus.getLocalBounds().width * 0.5f + 120.0f, roundStatus.getLocalBounds().height * 0.5f + 0.0f));

	sf::Text text0;
	text0.setFont(font);
	text0.setCharacterSize(12);
	text0.setFillColor(sf::Color::Red);
	text0.setStyle(sf::Text::Regular);
	//text0.setString("HEllo world");
	//set text origin
	text0.setOrigin(sf::Vector2f(text0.getLocalBounds().width * 0.5f + 120.0f, text0.getLocalBounds().height * 0.5f - 290.0f));

	sf::Text text3;
	text3.setFont(font);
	text3.setCharacterSize(12);
	text3.setFillColor(sf::Color::Red);
	text3.setStyle(sf::Text::Regular);
	//text0.setString("HEllo world");
	//set text origin
	text3.setOrigin(sf::Vector2f(text3.getLocalBounds().width * 0.5f -210.0f, text3.getLocalBounds().height * 0.5f - 290.0f));

	sf::Text text1;
	text1.setFont(font);
	text1.setCharacterSize(12);
	text1.setFillColor(sf::Color::Red);
	text1.setStyle(sf::Text::Regular);
	//text0.setString("HEllo world");
	//set text origin
	text1.setOrigin(sf::Vector2f(text1.getLocalBounds().width * 0.5f + 10.0f, text1.getLocalBounds().height * 0.5f - 290.0f));

	sf::Text text2;
	text2.setFont(font);
	text2.setCharacterSize(12);
	text2.setFillColor(sf::Color::Red);
	text2.setStyle(sf::Text::Regular);
	//text0.setString("HEllo world");
	//set text origin
	text2.setOrigin(sf::Vector2f(text2.getLocalBounds().width * 0.5f -100.0f, text2.getLocalBounds().height * 0.5f - 290.0f));

	//Create and Load Card Frame Texture
	sf::Texture cardFrame;
	cardFrame.loadFromFile("mycard.png");

	//Create and Load Card classes
	sf::Texture cardClassMelee;
	cardClassMelee.loadFromFile("classes/Melee.png");
	sf::Texture cardClassRange;
	cardClassMelee.loadFromFile("classes/Melee.png");
	sf::Texture cardClassSiege;
	cardClassMelee.loadFromFile("classes/Siege.png");

	//vector to hold cardCharacterTextures
	std::vector<sf::Texture*> cardCharacterTextures;
	std::vector<sf::Texture*> tempCardCharacterTextures;

	//Creating vector to hold sprites for cardFrame, cardClasses, and cardCharacters
	std::vector<sf::Sprite*> cardFrameSprite;
	std::vector<sf::Sprite> cardClassesSprite;
	std::vector<sf::Sprite*> cardCharactersSprite;
	std::vector<sf::Sprite*> boardCardCharactersSpritePlayer;
	std::vector<sf::Sprite*> boardCardCharactersSpriteComputer;
	std::vector<sf::Sprite*> cardFrameSpritePlayerBoard;
	std::vector<sf::Sprite*> cardFrameSpriteComputerBoard;
	
	//Creating vector to hold array of cardPositions on PlayerHand... values are hardcoded
	std::vector<std::vector<float>> cardFramePositions;
	std::vector<std::vector<float>> cardClassesPositions;
	//std::vector<std::vector<float>> cardTextPositions;

	cardFramePositions.push_back({110.0f, -250.0f});
	cardFramePositions.push_back({ 0.0f, -250.0f });
	cardFramePositions.push_back({ -110.0f, -250.0f });
	cardFramePositions.push_back({ -220.0f, -250.0f });

	cardClassesPositions.push_back({140.0f, -300.0f});
	cardClassesPositions.push_back({ 30.0f, -300.0f });
	cardClassesPositions.push_back({ -80.0f, -300.0f });
	cardClassesPositions.push_back({ -190.0f, -300.0f });
	while (window != nullptr) 
	{
		
		int roundNum = 0;

		while (!winner && window != nullptr)
		{
			//
			std::cout << "PlayerLives: " << playerLives << std::endl;
			std::cout << "Computer lives : " << computerLives << std::endl;
			int playerScore = 0;
			int computerScore = 0;
			roundNum += 1;

			

			if ((playerLives < 1) && (computerLives < 1)) {
				winner = true;
				std::cout << "No body wins" << std::endl;
				winnerStatus.setString("No body wins");
				window->draw(winnerStatus);
				window->display();
				Sleep(1000);
				window->clear();
				break;
			}
			else if (playerLives < 1) {
				winner = true;
				std::cout << "Computer wins" << std::endl;
				winnerStatus.setString("Computer wins");
				window->draw(winnerStatus);
				window->display();
				Sleep(1000);
				window->clear();
				break;
			}
			else if (computerLives < 1) {
				winner = true;
				std::cout << "player wins" << std::endl;
				winnerStatus.setString("player wins");
				window->draw(winnerStatus);
				window->display();
				Sleep(1000);
				window->clear();
				break;
			}

			bool playerPass = false;
			bool computerPass = false;
			bool roundOver = false;
			std::cout << "NEW ROUND----" << std::endl;

			roundStatus.setString("Round " + std::to_string(roundNum));
			window->draw(roundStatus);
			window->display();
			Sleep(1500);
			window->clear();

			while (!roundOver && window != nullptr) {
				sf::Event event;

				if (playerPass && computerPass)
				{
					roundOver = true;
					tempCardCharacterTextures.clear();
					boardCardCharactersSpritePlayer.clear();
					boardCardCharactersSpriteComputer.clear();
					cardFrameSpritePlayerBoard.clear();
					cardFrameSpriteComputerBoard.clear();


					if (playerScore > computerScore) {
						computerLives -= 1;
						std::cout << "PLAYER WON THE ROUND" << std::endl;
						roundStatus.setString("PLAYER WON THE ROUND");
						window->draw(roundStatus);
						window->display();
						Sleep(1500);
						window->clear();
					}
					else if (playerScore < computerScore) {
						playerLives -= 1;
						std::cout << "COMPUTER WON THE ROUND" << std::endl;
						roundStatus.setString("COMPUTER WON THE ROUND");
						window->draw(roundStatus);
						window->display();
						Sleep(1500);
						window->clear();
					}
					else {
						playerLives -= 1;
						computerLives -= 1;
						std::cout << "ROUND DRAW" << std::endl;
						roundStatus.setString("ROUND DRAW");
						window->draw(roundStatus);
						window->display();
						Sleep(1500);
						window->clear();
					}
					break;
				}
				
				
				
				
				

				bool turnOver = false;
				std::cout << "PLAYER CURRENT SCORE is: >>>>" << playerScore << std::endl;
				std::cout << "COMPUTER CURRENT score is: >>>>>>> " << computerScore << std::endl;
				
				while (!turnOver && window != nullptr)
				{
					
					std::cout << "You have these cards on Hand: " << std::endl;
					for (int i = 0; i < playerManager->getHand()->getHand().size(); i++)
					{
						std::string cardName = playerManager->getHand()->getHand()[i]->getCardName();
						int cardPower = playerManager->getHand()->getHand()[i]->getCartScore();
						std::cout << "Card name: " << playerManager->getHand()->getHand()[i]->getCardName() << std::endl;
						std::cout << "Card score: " << playerManager->getHand()->getHand()[i]->getCartScore() << std::endl;
						if (i == 0)	text0.setString(cardName+"\nPower: "+ std::to_string(cardPower));
						if (i == 1)	text1.setString(cardName + "\nPower: " + std::to_string(cardPower));
						if (i == 2)	text2.setString(cardName + "\nPower: " + std::to_string(cardPower));
						if (i == 3)	text3.setString(cardName + "\nPower: " + std::to_string(cardPower));

					}

					playerScoreText.setString("Player Score: " + std::to_string(playerScore));
					computerScoreText.setString("Computer Score: " + std::to_string(computerScore));
					
					int TEST = 0;
					//
					if (turn == 0)
					{
						bool playerPassClicked = false;
						
						cardCharacterTextures.clear();
						cardCharactersSprite.clear();
						cardFrameSprite.clear();
						window->clear();

						draw(playerManager, window, cardFrame, cardCharactersSprite, cardFrameSprite, cardCharacterTextures,
							cardFramePositions, boardCardCharactersSpritePlayer, boardCardCharactersSpriteComputer,
							cardFrameSpritePlayerBoard, cardFrameSpriteComputerBoard,
							tempCardCharacterTextures);
						window->draw(Wline);
						window->draw(text0);
						window->draw(text1);
						window->draw(text2);
						window->draw(text3);
						window->draw(playerScoreText);
						window->draw(computerScoreText);
						window->draw(passButton);
						window->display();

						if ((playerManager->getDeck()->getCardSet().size() == 0))
						{
							if (playerManager->getHand()->getHand().size() == 0)
							{
								std::cout << "PLAYER TURN AUTO PASS" << std::endl;
								playerPass = true;
								turnOver = true;
								turn = 1;
								break;
							}
						}
						
						while (window->waitEvent(event) && window != nullptr) //check for events occured in window while drawing
						{
							if (event.type == sf::Event::Closed) {

								window->close();
								delete window;
								window = nullptr;
								break;
							}
							if (event.type == sf::Event::MouseButtonPressed) {
								if (event.type == event.mouseButton.button == sf::Mouse::Left)
								{
									auto mousePosition = sf::Mouse::getPosition(*window); //mouse Pos relative to window
									auto mousePositionToWindow = window->mapPixelToCoords(mousePosition); //kind of same
									std::cout << mousePositionToWindow.x << "..." << mousePositionToWindow.y << std::endl;
									for (int i = 0; i < cardCharactersSprite.size(); i++) {
										//window->clear();
										
										if (cardFrameSprite[i]->getGlobalBounds().contains(mousePositionToWindow))
										{
											std::cout << cardFrameSprite[i]->getPosition().x << "..." << cardFrameSprite[i]->getPosition().y << std::endl;
											TEST = i;
										}
										else if (passButton.getGlobalBounds().contains(mousePositionToWindow)) {
											playerPassClicked = true;
										}
									}
									break;
								}
							}
						}
						

						std::string cardNumber;

						std::cout << "Player turn. Pick a card. enter 0,1,2,3; or P to pass" << std::endl;

						if (playerPassClicked)
						{
							std::cout << "You have passed your turn" << std::endl;
							playerPass = true;
							turnOver = true;
							turn = 1;
							break;
						}
						else
						{
							if (playerManager->getHand()->getHand().size() != 0)
							{
								card* card = playerManager->getHand()->drawCard(TEST);
								//draw 'drawcard' on the required position
								std::cout << "You have drawn card: " << card->getCardName() << std::endl;
								std::cout << "Card score is: " << card->getCartScore() << std::endl;
								std::cout << "-------------" << std::endl;
								playerScoreText.setString("Player Score: " + std::to_string(playerScore));
								
								playerScore += card->getCartScore();
								computerPass = false;
								sf::Sprite* boardSprite;
								sf::Sprite* boardFrame;
								boardSprite = cardCharactersSprite[TEST];
								boardFrame = cardFrameSprite[TEST];
								boardCardCharactersSpritePlayer.push_back(boardSprite);
								cardFrameSpritePlayerBoard.push_back(boardFrame);
							}
							if (playerManager->getDeck()->getCardSet().size() != 0)
							{
								playerManager->getHand()->addCard(playerManager->getDeck()->dealCard());

							}
							turnOver = true;
							turn = 1;
							break;
						}

					}
					else
					{
						int cardsPlayed = 0;
						if ((computerManager->getDeck()->getCardSet().size() == 0))
						{
							if (computerManager->getHand()->getHand().size() == 0)
							{
								std::cout << "COMPUTER TURN AUTO PASS" << std::endl;
								computerPass = true;
								turnOver = true;
								turn = 0;
								break;
							}
						}

						std::cout << "COMPUTER has these cards on Hand: " << std::endl;
						for (int i = 0; i < computerManager->getHand()->getHand().size(); i++)
						{
							std::cout << "Card name: " << computerManager->getHand()->getHand()[i]->getCardName() << std::endl;
							std::cout << "Card score: " << computerManager->getHand()->getHand()[i]->getCartScore() << std::endl;


						}

						if ((computerManager->getDeck()->getCardSet().size() == 0) ||
							(cardsPlayed == 6) ||
							(computerScore > 5) ||
							(abs(playerScore - computerScore) > 7))
						{
							std::cout << "COMPUTER has PASSED" << std::endl;
							computerPass = true;
							turnOver = true;
							turn = 0;
							break;
						}
						else
						{
							if (computerManager->getHand()->getHand().size() != 0)
							{
								int cardNum = rand() % (computerManager->getHand()->getHand().size());
								card* card = computerManager->getHand()->drawCard(cardNum);
								cardsPlayed += 1;
								std::cout << "Computer has drawn card: " << card->getCardName() << std::endl;
								std::cout << "Card score is: " << card->getCartScore() << std::endl;
								std::cout << "-------------" << std::endl;
								computerScoreText.setString("Computer Score: " + std::to_string(computerScore));
								computerScore += card->getCartScore();
								std::cout << cardNum << "CARD NUMB COMP" << std::endl;
								//set comp card texture
								sf::Texture* cardText = new sf::Texture;
								if (cardText->loadFromFile("characters/" + card->getCardName() + ".png") == false) {
									std::cout << "TEXTURE FAILED TO LOAD----------";
								}

								tempCardCharacterTextures.push_back(cardText);

								sf::Sprite* boardSprite = new sf::Sprite;
								sf::Sprite* boardFrame = new sf::Sprite;
								
								boardFrame->setTexture(cardFrame);

								boardCardCharactersSpriteComputer.push_back(boardSprite);
								cardFrameSpriteComputerBoard.push_back(boardFrame);
							}
							if (computerManager->getDeck()->getCardSet().size() != 0)
							{
								computerManager->getHand()->addCard(computerManager->getDeck()->dealCard());
							}
							turnOver = true;
							turn = 0;
							playerPass = false;
							break;
						}
					}
				}
			}
		}

	}
}

int main()
{
	srand(time(0));
	int playerWins = 0;
	int computerWins = 0;
	bool inPlay = true;
	std::cout << "Welcome to Gacha game. Start Play.";
	gameManager* playerManager = new gameManager;
	gameManager* computerManager = new gameManager;

	std::ifstream inputStream("cards.json");
	std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());

	json::JSON cardsDoc = json::JSON::Load(str);

	if (cardsDoc.hasKey("cards")) {
		playerManager->initialize(cardsDoc);
		computerManager->initialize(cardsDoc);
	}
	while (inPlay) {

		playGame(playerManager, computerManager);
		inPlay = false;
	}
	delete playerManager;
	delete computerManager;
}



