#pragma once
#include <vector>
#include "card.h"
#include "deck.h"

class hand
{

private:
	std::vector<card*> cardsHand;
public:
	hand();
	~hand();

	void initialize(deck* deck);
	std::vector<card*> getHand();
	void addCard(card* card);
	card* drawCard(int num);
};

