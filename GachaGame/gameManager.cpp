#include "gameManager.h"
#include "json.hpp"



gameManager::gameManager() : Deck(new deck), Hand(new hand)

{
	std::cout << "Game Manager created" << std::endl;
}

gameManager::~gameManager() {
	delete Deck;
	delete Hand;
	std::cout << "game manager destroyed" << std::endl;
}

void gameManager::initialize(json::JSON cardsData) {
	if (cardsData.hasKey("cards")) {
		Deck->initializeDeck(cardsData["cards"]);
	}
	Hand->initialize(Deck);
}

deck* gameManager::getDeck() {
	return Deck;
}

hand* gameManager::getHand() {
	return Hand;
}