#include "hand.h"


hand::hand() {

}

hand::~hand() {
	for (std::vector<card*>::iterator cardObj = cardsHand.begin(); cardObj != cardsHand.end(); ++cardObj)
	{
		delete (*cardObj);
	}
}

void hand::initialize(deck* deck) {
	for (int i = 0; i < 4; i++) {
		addCard(deck->dealCard());
	}
}
std::vector<card*> hand::getHand() {
	return cardsHand;
}

void hand::addCard(card* card) {
	cardsHand.push_back(card);
}

card* hand::drawCard(int num) {
	//card* tempCard = new card;
	card* tempCard;
	tempCard = cardsHand[num];
	cardsHand.erase(cardsHand.begin() + num);
	return tempCard;
}
