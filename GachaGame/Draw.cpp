#include "Draw.h"

Draw::Draw(gameManager* playerManager, sf::RenderWindow* window, sf::Texture cardFrame,
	std::vector<sf::Sprite*> cardCharactersSprite,
	std::vector<sf::Sprite*> cardFrameSprite, std::vector<sf::Texture*> cardTexture,
	std::vector<std::vector<float>> cardPositions) : playerManager(playerManager), window(window), cardFrame(cardFrame),
				cardCharactersSprite(cardCharactersSprite), cardFrameSprite(cardFrameSprite),
				cardTexture(cardTexture), cardPositions(cardPositions)
{

}

Draw::~Draw() {

}

void Draw::draw()
{
	for (int i = 0; i < playerManager->getHand()->getHand().size(); i++)
	{
		// CREATE textures for card characters for each card in hand
			// draw cards with set origin here
		float x_coord = cardPositions[i][0];
		float y_coord = cardPositions[i][1];

		//set and load texture
		sf::Texture* texture = new sf::Texture;
		if (texture->loadFromFile("characters/" + playerManager->getHand()->getHand()[i]->getCardName() + ".png") == false) {
			std::cout << "TEXTURE FAILED TO LOAD----------";
		}
		//create and load charactersprite
		sf::Sprite* cardSprite = new sf::Sprite;
		cardSprite->setTexture(*texture); //set texture in sprite
		cardSprite->setOrigin(sf::Vector2f(texture->getSize().x * 0.5f + x_coord, texture->getSize().y * 0.8f + y_coord));

		//create and load frame sprite
		sf::Sprite* frameSprite = new sf::Sprite;
		frameSprite->setTexture(cardFrame);
		frameSprite->setOrigin(sf::Vector2f(cardFrame.getSize().x * 0.5f + x_coord, cardFrame.getSize().y * 0.5f + y_coord));
		//push all components
		cardFrameSprite.push_back(frameSprite);
		cardTexture.push_back(texture);
		cardCharactersSprite.push_back(cardSprite);

		cardCharactersSprite[i]->setTexture(*texture);
		window->draw(*cardCharactersSprite[i]);
		window->draw(*cardFrameSprite[i]);
		window->display();
	}
}