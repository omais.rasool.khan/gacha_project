#include "card.h"
#include <iostream>

card::card()
{
	//std::cout << "Card created" << std::endl;
}

card::~card() {
	//std::cout << "card destroyed" << std::endl;
}

std::string card::getCardName()
{
	return cardName;
}

std::string card::getCardType() {
	return cardType;
}

void card::initializeCard(json::JSON cardInfo) {
	cardName = cardInfo["name"].ToString();
	cardType = cardInfo["type"].ToString();
	cardScore = cardInfo["score"].ToInt();
}

int card::getCartScore() {
	return cardScore;
}