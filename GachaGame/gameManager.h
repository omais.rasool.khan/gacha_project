#pragma once

#include "deck.h"
#include "hand.h"


class gameManager
{
private:
	
	deck* Deck;

	hand* Hand;
public:
	gameManager();
	~gameManager();
	
	hand* getHand();

	deck* getDeck();
	void initialize(json::JSON cardsData);
};

